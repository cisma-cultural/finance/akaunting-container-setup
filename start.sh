#/bin/sh

envsubst < env/db.env.example > env/db.env
envsubst < env/run.env.example > env/run.env

docker-compose up -d
